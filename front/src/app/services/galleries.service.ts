import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiGalleryData, Gallery, GalleryData } from '../models/gallery.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GalleriesService {
  constructor(private http: HttpClient) { }

  getGalleries(id: string) {
    return this.http.get<ApiGalleryData[]>(environment.apiUrl + `/galleries?user=${id}`).pipe(
      map(response => {
        return response.map(galleryData => {
          return new Gallery(
            galleryData._id,
            galleryData.user,
            galleryData.image,
            galleryData.title
          );
        });
      })
    );
  }

  getGallery(id: string) {
    return this.http.get<Gallery>(environment.apiUrl + `/galleries/${id}`).pipe(
      map(result => {
        return result
      })
    );
  }

  createGallery(galleryData: GalleryData) {
    const formData = new FormData();

    Object.keys(galleryData).forEach(key => {
      if (galleryData[key] !== null) {
        formData.append(key, galleryData[key]);
      }
    });

    return this.http.post(environment.apiUrl + '/galleries', formData);
  }

  removeGalleryImage(id: string) {
    return this.http.delete<ApiGalleryData[]>(environment.apiUrl + `/galleries/remove/${id}`).pipe(
      map(response => {
        return response.map(galleryData => {
          return new Gallery(
            galleryData._id,
            galleryData.user,
            galleryData.title,
            galleryData.image,
          );
        });
      })
    );
  }
}
