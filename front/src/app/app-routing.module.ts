import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { GalleriesComponent } from './pages/galleries/galleries.component';
import { NewGalleryImageComponent } from './pages/new-gallery-image/new-gallery-image.component';
import { UserGalleryComponent } from './pages/user-gallery/user-gallery.component';
import { ModalComponent } from './pages/modal/modal.component';

const routes: Routes = [
  {path: 'all-gallery', component: GalleriesComponent},
  {path: 'user-gallery/:id', component: UserGalleryComponent},
  {path: 'galleries/:id', component: ModalComponent},
  {path: 'new-gallery-image', component: NewGalleryImageComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
