import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ApiGalleryData, Gallery } from '../../models/gallery.model';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.sass']
})
export class ModalComponent implements OnInit {
  gallery: Observable<Gallery | null>;
  user: Observable<User | null>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  thisImage!: ApiGalleryData;
  constructor(private dialog: MatDialog,
              private store: Store<AppState>,
              private route: ActivatedRoute) {
    this.gallery = store.select(state => state.galleries.gallery);
    this.user = store.select(state => state.users.user);
    this.loading = store.select(state => state.galleries.fetchLoading);
    this.error = store.select(state => state.galleries.fetchError)
  }

  ngOnInit(): void {
    this.gallery.subscribe(gallery => {
      this.thisImage = <ApiGalleryData>gallery;
    });
  }

  onClose(){
    this.dialog.closeAll();
  }

}
