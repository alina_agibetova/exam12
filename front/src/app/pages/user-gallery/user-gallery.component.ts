import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Gallery } from '../../models/gallery.model';
import { User } from '../../models/user.model';
import { environment } from '../../../environments/environment';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { fetchUserGalleryRequest, removeGalleryImageRequest } from '../../store/gallery.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-gallery',
  templateUrl: './user-gallery.component.html',
  styleUrls: ['./user-gallery.component.sass']
})
export class UserGalleryComponent implements OnInit {
  galleries: Observable<null | Gallery[]>;
  gallerySub!: Subscription;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  user: Observable<null | User>;
  userSubscription!: Subscription;
  userId!: string;
  api = environment.apiUrl;
  userName!: string;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {
    this.user = store.select(state => state.users.user);
    this.galleries = this.store.select(state => state.galleries.galleries);
    this.loading = this.store.select(state => state.galleries.fetchLoading);
    this.error = this.store.select(state => state.galleries.fetchError)
  }

  ngOnInit(): void {
    this.route.params.subscribe( params => {
      this.store.dispatch(fetchUserGalleryRequest({id: params['id']}));
    });
    this.gallerySub = this.galleries.subscribe( card => {
      if (card) {
        this.userName = card[0]?.user.displayName
      }
    })
  }

  removeImage(id: string) {
    this.store.dispatch(removeGalleryImageRequest({id}))
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.gallerySub.unsubscribe();
  }

}
