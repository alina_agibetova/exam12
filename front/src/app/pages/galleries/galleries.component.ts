import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Gallery } from '../../models/gallery.model';
import { AppState } from '../../store/types';
import { GalleriesService } from '../../services/galleries.service';
import { fetchGalleriesRequest, fetchOneImageOfGalleryRequest } from '../../store/gallery.actions';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from '../modal/modal.component';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-galleries',
  templateUrl: './galleries.component.html',
  styleUrls: ['./galleries.component.sass']
})
export class GalleriesComponent implements OnInit {
  galleries: Observable<Gallery[]>;
  gallery: Observable<Gallery | null>;
  loading: Observable<boolean>;
  error: Observable<null | string>;
  modalOpen = false;
  user: Observable<User | null>;
  newUser!: User | null;

  constructor(private store: Store<AppState>,
              private route: ActivatedRoute,
              private galleriesService: GalleriesService,
              private dialog: MatDialog) {
    this.galleries = store.select(state => state.galleries.galleries);
    this.gallery = store.select(state => state.galleries.gallery);
    this.loading = store.select(state => state.galleries.fetchLoading);
    this.error = store.select(state => state.galleries.fetchError);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchGalleriesRequest());

  }

  openDialog(id: string) {
    this.store.dispatch(fetchOneImageOfGalleryRequest({id}));
    this.dialog.open(ModalComponent);
  }



}
