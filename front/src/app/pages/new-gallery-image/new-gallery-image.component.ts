import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Gallery } from '../../models/gallery.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { createGalleryImageRequest } from '../../store/gallery.actions';
import { User } from '../../models/user.model';

@Component({
  selector: 'app-new-gallery-image',
  templateUrl: './new-gallery-image.component.html',
  styleUrls: ['./new-gallery-image.component.sass']
})
export class NewGalleryImageComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  galleries: Observable<Gallery[]>;
  user: Observable<User | null>;
  newUser!: User | null;

  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.galleries.createLoading);
    this.error = store.select(state => state.galleries.createError);
    this.galleries = store.select(state => state.galleries.galleries);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.user.subscribe(user => {
      this.newUser = user;
    });
  }

  onSubmit() {
    const image = {
      user: <string>this.newUser?._id,
      title: this.form.value.title,
      image: this.form.value.image,
    }
    this.store.dispatch(createGalleryImageRequest({galleryData: image}));
  }
}
