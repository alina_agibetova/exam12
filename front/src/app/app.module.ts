import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserTypeDirective } from './directives/user-type.directive';
import { LayoutComponent } from './ui/layout/layout.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { GalleriesComponent } from './pages/galleries/galleries.component';
import { NewGalleryImageComponent } from './pages/new-gallery-image/new-gallery-image.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { MatMenuModule } from '@angular/material/menu';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CenterCardComponent } from './ui/center-card/center-card.component';
import { AuthInterceptor } from './auth.interceptor';
import { FacebookLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from 'angularx-social-login';
import { environment } from '../environments/environment';
import { MatCardModule } from '@angular/material/card';
import { AppStoreModule } from './app-store.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ImagePipe } from './pipes/image.pipe';
import { UserGalleryComponent } from './pages/user-gallery/user-gallery.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ModalComponent } from './pages/modal/modal.component';
import { FileInputComponent } from './ui/file-input/file-input.component';


const socialConfig: SocialAuthServiceConfig = {
  autoLogin: false,
  providers: [{
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider(environment.fbAppId, {
      scope: 'email, public_profile'
    })
  }
  ]
}

@NgModule({
  declarations: [
    AppComponent,
    UserTypeDirective,
    ImagePipe,
    LayoutComponent,
    GalleriesComponent,
    NewGalleryImageComponent,
    RegisterComponent,
    LoginComponent,
    CenterCardComponent,
    UserGalleryComponent,
    ModalComponent,
    FileInputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AppStoreModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    HttpClientModule,
    FormsModule,
    MatSnackBarModule,
    MatMenuModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    SocialLoginModule,
    MatProgressSpinnerModule,
    MatDialogModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: 'SocialAuthServiceConfig', useValue: socialConfig},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
