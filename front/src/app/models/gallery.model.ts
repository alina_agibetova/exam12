export class Gallery {
  constructor(
    public _id: string,
    public user: {
      _id: string,
      displayName: string
    },
    public image: string,
    public title: string
  ) {}
}

export interface GalleryData {
  [key: string]: any;
  user: string;
  title: string;
  image: File | null;
}

export interface ApiGalleryData {
  _id: string,
  title: string,
  user: {
    _id: string,
    displayName: string
  },
  image: string,
}
