import { createReducer, on } from '@ngrx/store';
import { GalleryState } from './types';
import {
  createGalleryImageFailure,
  createGalleryImageRequest, createGalleryImageSuccess,
  fetchGalleriesFailure,
  fetchGalleriesRequest,
  fetchGalleriesSuccess,
  fetchOneImageOfGalleryFailure,
  fetchOneImageOfGalleryRequest,
  fetchOneImageOfGallerySuccess,
  fetchUserGalleryFailure,
  fetchUserGalleryRequest,
  fetchUserGallerySuccess,
  removeGalleryImageRequest,
  removeGalleryImageSuccess
} from './gallery.actions';

const initialState: GalleryState = {
  gallery: null,
  galleries: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
};
export const galleryReducer = createReducer(
  initialState,
  on(fetchGalleriesRequest, state => ({...state, fetchLoading: true})),
  on(fetchGalleriesSuccess, (state, {galleries}) => ({...state, fetchLoading: false, galleries})),
  on(fetchGalleriesFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchOneImageOfGalleryRequest, state => ({...state, fetchLoading: true})),
  on(fetchOneImageOfGallerySuccess, (state, {gallery}) => ({...state, fetchLoading: false, gallery})),
  on(fetchOneImageOfGalleryFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(fetchUserGalleryRequest, state => ({...state, fetchLoading: true})),
  on(fetchUserGallerySuccess, (state, {galleries}) => ({...state, fetchLoading: false, galleries})),
  on(fetchUserGalleryFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(createGalleryImageRequest, state => ({...state, createLoading: true})),
  on(createGalleryImageSuccess, state => ({...state, createLoading: false})),
  on(createGalleryImageFailure, (state, {error}) => ({...state, createLoading: false, createError: error,})),

  on(removeGalleryImageRequest, state => ({...state, fetchLoading: true})),
  on(removeGalleryImageSuccess, (state, {galleries}) => ({...state, fetchLoading: false, galleries})),
);
