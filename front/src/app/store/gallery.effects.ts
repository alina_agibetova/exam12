import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of, tap } from 'rxjs';
import { Router } from '@angular/router';
import { HelpersService } from '../services/helpers.service';
import { GalleriesService } from '../services/galleries.service';
import {
  createGalleryImageFailure,
  createGalleryImageRequest, createGalleryImageSuccess,
  fetchGalleriesFailure,
  fetchGalleriesRequest,
  fetchGalleriesSuccess,
  fetchOneImageOfGalleryFailure,
  fetchOneImageOfGalleryRequest,
  fetchOneImageOfGallerySuccess,
  fetchUserGalleryFailure,
  fetchUserGalleryRequest,
  fetchUserGallerySuccess,
  removeGalleryImageRequest,
  removeGalleryImageSuccess
} from './gallery.actions';

@Injectable()
export class GalleryEffects {
  constructor(
    private actions: Actions,
    private galleriesService: GalleriesService,
    private router: Router,
    private helpers: HelpersService,
  ) {}

  fetchGalleries = createEffect(() => this.actions.pipe(
    ofType(fetchGalleriesRequest),
    mergeMap(() => this.galleriesService.getGalleries('').pipe(
      map(galleries => fetchGalleriesSuccess({galleries})),
      catchError(() => of(fetchGalleriesFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchGalleryOfOneImage = createEffect(() => this.actions.pipe(
    ofType(fetchOneImageOfGalleryRequest),
    mergeMap(({id}) => this.galleriesService.getGallery(id).pipe(
      map(gallery => fetchOneImageOfGallerySuccess({gallery})),
      catchError(() => of(fetchOneImageOfGalleryFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  fetchUserGalleries = createEffect(() => this.actions.pipe(
    ofType(fetchUserGalleryRequest),
    mergeMap(({id}) => this.galleriesService.getGalleries(id).pipe(
      map(galleries => fetchUserGallerySuccess({galleries})),
      catchError(() => of(fetchUserGalleryFailure({
        error: 'Something went wrong'
      })))
    ))
  ));

  createGallery = createEffect(() => this.actions.pipe(
    ofType(createGalleryImageRequest),
    mergeMap(({galleryData}) => this.galleriesService.createGallery(galleryData).pipe(
      map(() => createGalleryImageSuccess()),
      tap(() => this.router.navigate(['/'])),
      catchError(() => of(createGalleryImageFailure({error: 'Wrong data'})))
    ))
  ));

  removeImageOfGallery = createEffect(() => this.actions.pipe(
    ofType(removeGalleryImageRequest),
    mergeMap(({id}) => this.galleriesService.removeGalleryImage(id).pipe(
      map(galleries => removeGalleryImageSuccess({galleries})),
      tap(() => {
        this.helpers.openSnackbar('Cocktail is delete!');
      })
    ))
  ));
}
