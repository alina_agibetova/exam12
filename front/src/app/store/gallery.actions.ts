import { createAction, props } from '@ngrx/store';
import { Gallery, GalleryData } from '../models/gallery.model';

export const fetchGalleriesRequest = createAction('[Galleries] Fetch Request');
export const fetchGalleriesSuccess = createAction('[Galleries] Fetch Success', props<{galleries: Gallery[]}>());
export const fetchGalleriesFailure = createAction('[Galleries] Fetch Failure', props<{error: string}>());

export const fetchOneImageOfGalleryRequest = createAction('[Galleries] FetchIng Request', props<{id: string}>());
export const fetchOneImageOfGallerySuccess = createAction('[Galleries] FetchIng Success', props<{gallery: Gallery}>());
export const fetchOneImageOfGalleryFailure = createAction('[Galleries] FetchIng Failure', props<{error: string}>());

export const fetchUserGalleryRequest = createAction('[Galleries] Fetch User Request', props<{id: string}>());
export const fetchUserGallerySuccess = createAction('[Galleries] Fetch User Success', props<{galleries: Gallery[]}>());
export const fetchUserGalleryFailure = createAction('[Galleries] Fetch User Failure', props<{error: string}>());

export const createGalleryImageRequest = createAction('[Galleries] Create Request', props<{galleryData: GalleryData}>());
export const createGalleryImageSuccess = createAction('[Galleries] Create Success');
export const createGalleryImageFailure = createAction('[Galleries] Create Failure', props<{error: string}>());

export const removeGalleryImageRequest = createAction('[Galleries] Remove Request', props<{id: string}>());
export const removeGalleryImageSuccess = createAction('[Galleries] Remove Success', props<{galleries: Gallery[]}>());
