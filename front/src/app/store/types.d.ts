import { LoginError, RegisterError, User } from '../models/user.model';
import { Gallery } from '../models/gallery.model';

export type UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
}

export type GalleryState = {
  gallery: null | Gallery,
  galleries: Gallery[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
}



export type AppState = {
  users: UsersState,
  galleries: GalleryState
}
