const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, 'public/uploads'),
  mongo: {
    db: 'mongodb://localhost/exam12',
    options: {useNewUrlParser: true},
  },
  facebook: {
    appId: '399750588242601',
    appSecret: '424901eb33886d1ac10870717b9ac45c'
  }
}

