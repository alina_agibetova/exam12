const path = require('path');
const fs = require("fs").promises;
const express = require('express');
const multer = require('multer');
const { nanoid } = require('nanoid');
const config = require('../config');
const mongoose = require("mongoose");
const Gallery = require("../models/Gallery");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try {
    let query = {}
    if (req.query.user) {
      query.user = {_id: req.query.user}
    }

    const galleryId = await Gallery.find(query).populate("user", "displayName");
    return res.send(galleryId);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try {
    const gallery = await Gallery.findById(req.params.id);

    if (!gallery) {
      return res.status(404).send({message: 'Not found'});
    }

    return res.send(gallery);
  } catch (e) {
    next(e);
  }
});

router.post('/', auth, permit('user'), upload.single('image'), async (req, res, next) => {
  try {
    const galleryData = {
      title: req.body.title,
      user: req.body.user,
      image: null,
    };

    if (req.file) {
      galleryData.image = req.file.filename;
    }
    const gallery = new Gallery(galleryData);

    await gallery.save();

    return res.send({message: 'Created new image', id: gallery._id});
  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      if (req.file) {
        await fs.unlink(req.file.path);
      }
      return res.status(400).send(e);
    }
    next(e);
  }
});

router.delete('/remove/:id', auth, permit('user'), async (req, res, next) => {
  try {
    await Gallery.deleteOne({_id: req.params.id});
    const galleries = await Gallery.find().populate("user", "displayName");

    return res.send(galleries);
  } catch (e) {
    next(e);
  }
});

module.exports = router;