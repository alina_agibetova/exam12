const mongoose = require('mongoose');
const config = require('./config');
const User = require('./models/User');
const {nanoid} = require('nanoid');
const Gallery = require("./models/Gallery");

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    email: 'user@test.com',
    password: '123',
    token: nanoid(),
    role: 'user',
    displayName: 'user',
    avatar: 'no_image_available.jpg'
  }, {
    email: 'admin@test.com',
    password: '123',
    token: nanoid(),
    role: 'admin',
    displayName: 'admin',
    avatar: 'no_image_available.jpg'
  })

  await Gallery.create({
    user: user,
    image: 'маргарита.jpg',
    title: 'Margarita',
  }, {
    user: user,
    image: 'лимонад.jpeg',
    title: 'Limonade',
  }, {
    user: admin,
    image: 'молочный.jpeg',
    title: 'Milk shake',
  }, {
    user: admin,
    image: 'текила.jpeg',
    title: 'Tekila',
  });

  await mongoose.connection.close();
}

run().catch(e => console.error(e))